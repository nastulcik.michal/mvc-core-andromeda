<?php

namespace App\CoreModule\User\Controllers;

use App\CoreModule\System\Controllers\EmailSenderController;
use App\CoreModule\User\Models\UserManager;
use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use Utils\UserException;
use Utils\HtmlBuilder;
use Utils\Forms\Form;
use Utils\EmailSender;
use Settings;

/**
 * Process request on reset password
 */
class ResetPasswordController extends Controller
{
    /**
     * Manager for article
     * @var ArticleManager
     */
    public $articleManager;

    /**
     * Class for manage user
     * @var UserManager
     */
    public $userManager;

    function __construct(
        ArticleManager $articleManager,
        UserManager $userManager
    )
    {
        $this->articleManager = $articleManager;
        $this->userManager = $userManager;
    }

    /**
     * @Action
     * Send email with reset link for change forget password
     */
    public function index()
    {
        $form = $this->resetPasswordForm();
        $this->data['form'] = $form;

        $domain = $_SERVER['SERVER_NAME'];

        if ($form->isPostBack())
        {
            try
            {
                $data = $form->getData();
                $address = $data['email'];
                $emailSender = new EmailSenderController();
                $subject = $this->translate("Reset hesla na webu") ." $domain";
                $resetLink = 'http://'.$domain . "/zapomenute-heslo/link/" . $this->userManager->generatePasswordResetLink($address);
                $message = 'Po kliknutí na <a href="_RESET_"> odkaz </a> budete přesměrování na stránku pro změnu hesla.';

                // $translate = $this->translate('Email - Reset hesla na webu');
                // if($translate!=$message) $message = $translate;

                $message = str_replace("_RESET_", $resetLink, $message);
                $emailSender->sentEmailSMTP("info@sex-czech.cz", $address, $subject, $message);

                $this->addMessage('Email s novým heslem byl úspěšně odeslán');
                $this->redirect('prihlaseni');
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage());
            }
        }

        // Naplnění proměnných pro šablonu
        $this->data['title'] = $this->articleManager->article['title'];
        $this->data['content'] = $this->articleManager->article['content'];
        $this->data['resetPasswordForm'] = $this->resetPasswordForm();

        // Nastavení šablony
        $this->view = 'index';
    }

    /**
     * @Action
     * Action for reset password by reset link (link sended in email)
     */
    public function link($resetLink)
    {
        if (empty($resetLink)) {
            $this->redirect('chyba');
        }

        // Control if exist user with identic reset link
        $user = $this->userManager->getUserByResetLink($resetLink);

        // Doesnt exist user with send reset link - redirect
        if (!$user) {
            $this->addMessage("Platnost odkazu pro změnu hesla vypršela", 'danger');
            $this->redirect("/");
        }

        $form = $this->changePasswordForm();
        $this->data['form'] = $form;

        if ($form->isPostBack())
        {
            try
            {
                $data = $form->getData();
                $data['user_id'] = $user['id'];
                $this->userManager->passwordReset($data);
                $emailSender = new EmailSender();
                $emailContent = 'Váše heslo k účtu na webu ' . Settings::$domain ." bylo úspěšně změněno." ;
                $emailSender->send($user['email'], "Uspěšný reset hesla na webu " .  Settings::$domain, $emailContent, "sex-czech@info.cz");
                $this->addMessage('Heslo bylo úspěšně změněno');
                $this->redirect('prihlaseni');
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage(), 'danger');
            }
        }

        $this->data['title'] = $this->articleManager->article['title'];
        $this->data['content'] = $this->articleManager->article['content'];
        $this->data['changePasswordForm'] = $this->changePasswordForm();

        $this->view = 'link';
    }

    public function resetPasswordForm()
    {
        $form = new Form('reset-password-form');
        $form->disableLabel();
        $form->addEmailBox('email', 'Email:', true, ['class' => 'form-control-sm','placeholder' => "papa.travnicek@pepovo.cz"]);
        $form->addButton('submit' , 'Obnovit heslo', ['class' => 'btn-sm btn-warning rounded-pill float-right']);
        
        return $form;
    }

    public function changePasswordForm()
    {
        $form = new Form('change-password');

        $form->addPasswordBox('new_password', $this->translate('Nové heslo'), true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");

        $form->addPasswordBox('new_password_repeat', $this->translate('Potvrzení hesla'), true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");

        $form->addButton('submit' , $this->translate('Změnit heslo'), ['class' => 'btn-lg btn-block btn-warning rounded-pill mt-3']);

        return $form;
    }
}