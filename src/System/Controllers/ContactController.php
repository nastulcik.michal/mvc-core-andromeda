<?php

namespace App\CoreModule\System\Controllers;

use Utils\EmailSender;
use Utils\UserException;
use Utils\Forms\Form;
use Settings;

/**
 * Process contact form
 */
class ContactController extends Controller
{
    /**
     * Contact
     * @Action
     */
    public function index()
    {
        $form = $this->getContactForm();
        $this->data['form'] = $form;

        if ($form->isPostBack())
        {
            try
            {
                $data = $form->getData();
                $emailSender = new EmailSender();
                $emailSender->send(Settings::$email, "Email z webu - " .  Settings::$domain, $data['message'], $data['email']);
                $this->addMessage('Email byl úspěšně odeslán.');
                $this->redirect('kontakt');
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage());
            }
        }

        $this->view = 'index';
    }

    /**
     * Return contact form
     * @return Form Contact form
     */
    public function getContactForm()
    {
        $form = new Form('contact');
        $form->addHiddenBox('article_id');
        $form->addTextBox('email', 'Vaše emailová adresa', true, ['class' => "form-control"]);
        $form->addTextBox('message', 'Zpráva', true, ['class' => "form-control"])->addMinLengthRule(10);
        $form->addRecaptchaBox(Settings::$recaptcha['secret_key'], Settings::$recaptcha['site_key'], 0.5);
        $form->addButton('submit' , 'Odeslat', ['class' => "form-control"]);
        return $form;
    }
}