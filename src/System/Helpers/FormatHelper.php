<?php 

use Utils\Utility\DateUtils;
use Utils\Utility\StringUtils;

/**
 * Helper with format methods
 */
class FormatHelper
{
    public static function capitalize($text)
    {
        return StringUtils::capitalize($text);
    }

    public static function shorten($text, $length)
    {
        return StringUtils::shorten($text, $length);
    }

    public static function prettyDate($date, $time = true)
    {
        return DateUtils::prettyDate($date, $time);
    }

    public static function prettyDateTime($date)
    {
        return DateUtils::prettyDateTime($date);
    }

    public static function numericDate($date)
    {
        $dateTime = new DateTime($date);
        return $dateTime->format("j.n. Y");
    }

    public static function currency($price, $currency = 'Kč')
    {
        return number_format($price, 2, ',', ' ') . ' ' . $currency;
    }

    public static function boolean($value)
    {
        return $value ? 'Ano' : 'Ne';
    }
}