<?php 
/**
 * DIC - dependency injection construct typ = CONSTRUCTOR INJECTION
 */
class DependencyInjectionConstruct
{
	private $services = [];

    public $debug = false;

    /**
     * Prida vytvorenou tridu do pole pro budouci pouziti
     * @param object $service Class instance
     */
	public function addService($service)
	{
        $key = get_class($service);
        if ($this->debug) {
            var_dump("Add new instance for class: " . $key ."</br>");
        }
        $this->services[$key] = $service;
	}

    /**
     * Vrátí instanci podle namespace+trida         
     * @param  string $fullClassName Celý název třídy
     * @return object                Instance volané třídy
     */
	public function returnInstance($fullClassName)
	{
        // Vytvoření instance
        if (!isset($this->services[$fullClassName]))
        {
            $reflection = new \ReflectionClass($fullClassName);
            $constructor = $reflection->getConstructor();

            // Kontrola zda ma trida constructor
            if ($constructor) {
                $params = $constructor->getParameters();

                // Kontrola zda ma konstruktor parametry
                if ($params) {
                    $dependencies = $this->getDependencies($params);
                    if ($dependencies) {
                        $instance = $reflection->newInstanceArgs($dependencies);
                        $this->addService($instance);
                        return $instance;
                    }
                }
            }
            $instance = new $fullClassName();
            $this->addService($instance);
            return $instance;
        }
        if ($this->debug) {
            var_dump("Return class: " . $fullClassName . "</br>");
        }
        return $this->services[$fullClassName];
	}

    /**
     * Vrátí seznam závislostí
     * @param  $params
     * @return array
     * @throws Exception
     */
	private function getDependencies($params)
	{
        $dependencies = [];
        foreach ($params as $param) {
            // Vrati jmeno potrebne tridy 
            $dependency = $param->getClass();
            if ($dependency === NULL) {
                // Kontrola zda je trida dostupna - promenna v kontruktoru bez nazvu tridy
                if (!$param->isDefaultValueAvailable()) {
                    throw new Exception("Nelze vyresit zavislost pro {$param->name}");
                }
            } else {
                // Vrati instanci podle jmeno potrebne tridy
                $dependencies[] = $this->returnInstance($dependency->name);
            }
        }
        
        return $dependencies;
	}
}