<?php

namespace App\CoreModule\System\Controllers;

use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\User\Models\UserManager;
use Utils\Utility\StringUtils;
use Utils\UserException;
use TranslateHelper;
use ReflectionClass;
use Exception;
use ReflectionException;
use ReflectionMethod;
use Settings;

/**
 * Parent for all controllers in application
 */
abstract class Controller
{
    const MSG_INFO = 'info';
    const MSG_SUCCESS = 'success';
    const MSG_ERROR = 'danger';
    const MSG_WARNING = 'warning';

    /**
     * @var bool If controller create API no article
     */
    protected $createdByApi;

    /**
     * @var array Array with data -> For template translate indexes to variable
     */
    protected $data = array();
    /**
     * @var string Template name without suffix
     */
    protected $view = "";

    /**
     * Path to folder with template
     * @var string
     */
    protected $templatePath;

    /**
     * Treate variable for extract into HTML page
     * @param mixed $x treating variable
     * @return mixed  treated variable
      */
    private function protect($x = null)
    {
        if (!isset($x))
            return null;
        elseif (is_string($x))
            return htmlspecialchars($x, ENT_QUOTES);
        elseif (is_array($x))
        {
            foreach($x as $k => $v)
            {
                $x[$k] = $this->protect($v);
            }
            return $x;
        }
        else
            return $x;
    }

    /**
     * Control if user is login, if not redirect to "prihlaseni"
     */
    public function authUser()
    {
        if (!isset($_SESSION['user'])) {
            $this->addMessage("Pro navštívení této stránky musíte být přihlášen");
            $this->redirect('prihlaseni');
        }
    }

    /**
     * Control if user is admin
     */
    public function authAdminUser()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user']['admin'] != 1) {
            $this->addMessage("Pro navštívení této stránky musíte být admin");
            $this->redirect('prihlaseni');
        }
    }

    public function getUserId()
    {
        if (isset($_SESSION['user']['id'])) {
            return $_SESSION['user']['id'];
        }
    }

    public function userLogin()
    {
        if (!isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    public function logOutUser()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            $this->addMessage("Byl jste úspěšně odhlášen");
            $this->redirect('/');
        }   
    }

    /**
     * Render view
     */
    public function renderView()
    {
        if ($this->view)
        {
            extract($this->protect($this->data));
            extract($this->data, EXTR_PREFIX_ALL, "");
            $trans = TranslateHelper::getLibraryClass();
            $reflect = new ReflectionClass(get_class($this));
            $path = str_replace('Controllers', 'Views', str_replace('\\', '/', $reflect->getNamespaceName()));
            $controllerName = str_replace('Controller', '', $reflect->getShortName());
            $e = explode("/", $path);
            
            // Create path to controller
            if ($e[1] == "CoreModule") {
                if ($this->view == "layout") {
                    $path = Settings::$layoutTemplatePath;
                } else {
                    $this->templatePath = '../vendor/mvc/core-andromeda/src/' . $e[2] ."/". $e[3]. "/" . $controllerName . '/';
                    $path = $this->templatePath . $this->view . '.phtml';
                }
            } else {
                $this->templatePath = '../a' . ltrim($path, 'A') . '/' . $controllerName . '/';
                $path = $this->templatePath . $this->view . '.phtml';
            }
            require($path);
        }
    }

    /**
     * Return path to template by actual controller
     * @return string Path to template folder
     */
    public function getTemplatePath()
    {
        if ($this->templatePath) {
            return $this->templatePath;
        }

        $reflect = new ReflectionClass(get_class($this));
        $path = str_replace('Controllers', 'Views', str_replace('\\', '/', $reflect->getNamespaceName()));
        $controllerName = str_replace('Controller', '', $reflect->getShortName());
        $e = explode("/", $path);

        if ($e[1] == "CoreModule") {
            $this->templatePath = '../vendor/mvc/core-andromeda/src/' . $e[2] ."/". $e[3]. "/" . $controllerName . '/';
        } else {
            $this->templatePath = '../a' . ltrim($path, 'A') . '/' . $controllerName . '/';
        }

        return $this->templatePath;
    }

    public function getBaseTemplatePath(): string
    {
        if ($this->templatePath) {
            return $this->templatePath;
        }

        $reflect = new ReflectionClass(get_class($this));
        $path = str_replace('Controllers', 'Views', str_replace('\\', '/', $reflect->getNamespaceName()));
        $e = explode("/", $path);

        if ($e[1] == "CoreModule") {
            $this->templatePath = '../vendor/mvc/core-andromeda/src/' . $e[2] ."/". $e[3]. "/";
        } else {
            $this->templatePath = '../a' . ltrim($path, 'A') . '/';
        }

        return $this->templatePath;
    }

    /**
     * Load template from actual controller folder
     * @param   $name     TemplateName
     */
    public function loadTemplate($name, $data)
    {
        // Load translate for template
        $trans = TranslateHelper::getLibraryClass();

        // Load data for template
        extract($this->protect($data));
        extract($data, EXTR_PREFIX_ALL, "");

        require($this->getTemplatePath() . $name .  ".phtml");
    }

    public function translate($text)
    {
        return TranslateHelper::get($text);        
    }

    /**
     * Call required action on controller by parameters
     * @param  array  $params Params
     * @param  boolean $mode  Actual mode - Action,ApiAction,CliAction
     * @return void
     */
    public function callActionFromParams($params , $mode = "Action")
    {
        // Getting the called action
        $action = StringUtils::hyphensToCamel($params ? array_shift($params) : 'index');

        // Getting information about method
        try
        {
            $method = new ReflectionMethod(get_class($this), $action);
        }
        catch (ReflectionException $exception)
        {
            throw new Exception("Neznámá akce - $action");
        }

        // Control access
        $phpDoc = $method->getDocComment();
        $annotation = '@'.$mode;

        if (mb_strpos($phpDoc, $annotation) === false){
            throw new Exception("Neplatná akce - $action");
        }

        $requiredParamsCount = $method->getNumberOfRequiredParameters();

        // Control params for calling action
        if (count($params) < $requiredParamsCount){
            $this->throwRoutingException("Akci nebyly předány potřebné parametry ($requiredParamsCount)");

        }

        // Insert arguments into calling action
        $method->invokeArgs($this, $params);
    }

    /**
     * Add message for user
     * @param string $content Content message
     * @param string $type    Message type
     */
    public function addMessage($content, $type = self::MSG_INFO)
    {
        $message = array(
            'content' => $content,
            'type' => $type,
        );
        if (isset($_SESSION['messages']))
            $_SESSION['messages'][] = $this->translate($message);
        else
            $_SESSION['messages'] = array($message);
    }

    /**
     * Return messages for user
     * @return array Messages
     */
    public function getMessages()
    {
        if (isset($_SESSION['messages']))
        {
            $messages = $_SESSION['messages'];
            unset($_SESSION['messages']);
            return $messages;
        }
        else {
            return array();
        }
    }

    /**
     * Redirect on url
     * @param string $url URL
     */
    public function redirect($url = '')
    {
        if (!$url) {
            $url = ArticleManager::$article['url'];
        }

        // Redirect on homePage
        if ($url == "/") {
            header("Location: /");
            header("Connection: close");
            exit;    
        }
        header("Location: /$url");
        header("Connection: close");
        exit;
    }

    /**
     * In debug mode throws an exception, otherwise redirect on 404
     * @param string $message Debug message
     * @throws Exception
     */
    private function throwRoutingException($message)
    {
        if (Settings::$debug)
            throw new Exception($message);
        else
            $this->redirect('chyba');
    }
}