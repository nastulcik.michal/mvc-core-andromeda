<?php

namespace App\CoreModule\System\Controllers;

use App\CoreModule\System\Controllers\EmailSenderController;
use Utils\UserException;
use Utils\Forms\Form;
use Settings;

 // require_once('phpmailer/Exception.php');
 //         require_once('phpmailer/PHPMailer.php');
 //         require_once('phpmailer/SMTP.php');

/**
 * Testing controller
 */
class TestController extends Controller
{
    /**
     * Test
     * @Action
     */
    public function index()
    {
        $this->sentEmail();
        $this->view = 'index';
    }

    public function sentEmail()
    {
        $emailSender = new EmailSenderController();

        $address = "nastulcik.michal@gmail.com";
        $verificationLink = "http://".Settings::$domain . "/administrace/control-verification-email-link/blablablimk";
        $subject = "Nová regitrace na portálu sex-czech.cz";
        $message = '
            <p>
                Vážený zákazníku, 
            </p>
            <p>
                děkujeme Vám za Vaši registraci.
            </p>
            
            <p>
                Na našem portálu <a href="www.sex-czech.cz">www.sex-czech.cz</a> Vám bylo vytvořeno uživatelské konto, které poskytuje řadu výhod. Vaše údaje byly uloženy, což Vám ušetří čas při dalším přihlášení, kde už zadáte pouze přihlašovací údaje. 
            </p>
            <p>
                Po přihlášení na našem webu: 
                <a href="http://sex-czech.cz/prihlaseni">sex-czech.cz/prihlaseni</a>,
                získáte přístup k Vašim inzerátům, které můžete editovat, přidávat další nebo je smazat. 
            </p>
            
            <p>
                Vaše přihlašovací mailová adresa: test@tesst.cz
            </p>
            <p>
                Vaše přihlašovací heslo: nereknu   
            </p>

            <p>
                Pro potvrzení emailu klikněte prosím na tento odkaz <a href="' . $verificationLink . '"> odkaz </a>.
            </p>
            
            <p>
                S pozdravem <br>
                Diamond public s.r.o.<br>
                info@diamond-public.cz
            </p>';

        try {
            $emailSender->sentEmailSMTP($address, $subject, $message);
            $this->addMessage('Email byl úspěšně odeslán.');
        } catch (UserException $ex)
        {
            $this->addMessage($ex->getMessage());
        }
    }
}