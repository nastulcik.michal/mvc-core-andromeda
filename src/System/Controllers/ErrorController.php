<?php

namespace App\CoreModule\System\Controllers;

/**
 * Proccess error message
 */
class ErrorController extends Controller
{
    /**
     * Send error message
     * @Action
     */
    public function index()
    {
        // Hlavička požadavku
        header("HTTP/1.0 404 Not Found");
    }
}