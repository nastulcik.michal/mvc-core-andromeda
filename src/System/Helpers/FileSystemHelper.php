<?php 

use Utils\Utility\StringUtils;
use Utils\UserException;
use Utils\Image;

/**
 * Class for work with filesystem - save/delete - file/folder
 */
class FileSystemHelper
{
    // Project path - root path
    public static $projectDir;

    // Class for work with image
    public static $image;

    // Work in cli
    public $cli = 0;

    function __construct($projectDir)
    {
        if (!isset(self::$projectDir))
        {
            self::$projectDir = $projectDir;
        }
        return self::$projectDir;
    }

    public function initImageClass()
    {
        if (!isset(self::$image))
        {
            self::$image = new Image();
        }
        return self::$image;
    }

    public function getFile($path)
    {
        return fopen(self::$projectDir.$path, "rw");
    }

    public static function getProjectDir()
    {
        return self::$projectDir;
    }

    public function getFileContents($path)
    {
        return ($this->exist($path)) ? file_get_contents(self::$projectDir.$path,"rw") : false;
    }

    public function deleteFile($filePath)
    {
        if ($this->exist($filePath)) {
            echo self::$projectDir.$filePath;
            unlink(self::$projectDir.$filePath);
        }
    }

    public function save($path, $content)
    {
        // check if is string path or not
        $split_path = explode("/", $path);

        if (count($split_path)>0) {
            // get file name
            $file_name = array_pop($split_path);

            //only directories path
            $fpath = str_replace($file_name,"",$path);

            //check if exist folder/path
            if (!file_exists(str_replace($file_name,"",$path))) {
                //create directories path
                $this->createDirPath(array_reverse($split_path));
            }

            //save file - if file exist => rewrite file
            if ($this->cli) {
                Printer::cli("File SAVE TO: ".self::$projectDir.$fpath.$file_name,'sucess');
            }

            file_put_contents(self::$projectDir.$fpath.$file_name,$content);
            return;
        }

        //save file - if file exist => rewrite file
        if ($this->cli) {
            Printer::cli("REWRITE file: ".self::$projectDir.$fpath.$file_name,'warning');
        }
        file_put_contents($fpath.$file_name,$content);
        chmod($fpath.$file_name, 0777);
    }

    public function olderThen($time, $path)
    {
        if (!$this->exist($path)) return false;

        if (filemtime(self::$projectDir.$path) > $time)
            return true;
        return false;
    }

    public function newerThen($time, $path)
    {
        if (!$this->exist($path)) return false;

        if (filemtime(self::$projectDir.$path) < $time)
            return true;
        return false;
    }

    // Control if file path or folder exist
    public function exist($path)
    {
        if (file_exists(self::$projectDir.$path)) {
            return true;
        }
        return false;
    }

    public function createDir($path)
    {
        // check if is string path or not
        $split_path = explode("/", $path);

        //create directories path
        $this->createDirPath(array_reverse($split_path));
    }

    // create recursive folder path
    private function createDirPath($list,$fpath=false)
    {
        // get first name for dir
        $dir_name = array_pop($list);

        //create path for create new directory
        if (!$fpath) {
            $fpath = self::$projectDir.$dir_name;
        }else{
            if ($fpath===true)$fpath="/";
            $fpath .= $dir_name;
        }

        //create directory + check if folder already exist
        if (!file_exists($fpath)) {
            var_dump($fpath);
            mkdir($fpath);
        }

        // check if list have some directory for create
        if (count($list)>0) {
            // call again function for create directory
            $this->createDirPath($list,$fpath."/");
        }

        $fpath.="/";
        return $fpath;
    }

    public function scandir($dir)
    {
        $directory = self::$projectDir . $dir;
        return array_diff(scandir($directory), array('..', '.'));
    }

    /**
     * Control and save upload image 
     * @param  array   $imageFile   Image data
     * @param  string  $saveFolder  Name for folder where will be save image
     * @param  boolean $subfolder   Create subfolder from first char of fileName
     * @return string               Return image save path
     */
    public function processImage($imageFile, $saveFolder, $subfolder = true)
    {
        $this->initImageClass();
        $this->controlImage($imageFile);
        $this->loadImage($imageFile['tmp_name']);

        // Explode name by "."  - [0] == name , [1] == "image format"
        $exp = explode(".", $imageFile['name']);
        $fileName = StringUtils::hyphenize($exp[0]);

        $folderPath = $this->getFilePath() . "/webp-images/" . $saveFolder;

        if ($subfolder) {
            $folderPath .= "/" . $fileName[0];
        }

        // Create folder if dont exist
        if (!$this->exist($folderPath)) {
            $this->createDir($folderPath);
        }

        $imageTypeSuffix = ".jpg";
        $filePath = $folderPath . "/" . $fileName;
        $filePathSuffix = $filePath . $imageTypeSuffix;

        $fileFullPath = self::$projectDir . $filePath;
        $fileFullPathSuffix = self::$projectDir . $filePathSuffix;

        self::$image->save($fileFullPathSuffix);

        $this->convertImageToWebP($fileFullPathSuffix, $fileFullPath . ".webp");
        
        return $filePathSuffix;
    }

    /**
     * Create image by dimension - max height
     * @param  string $origImgPath       Path to original image
     * @param  array  $dimensions        Array with image dimensions -> key = name, value = height dimension
     * @return void                      Create new image by dimensions array
     */
    public function createImageDimensions($origImgPath, $dimensions)
    {
        $this->initImageClass();
        $origImgPathNoExt = str_replace(".jpg", "", $origImgPath);
        $imgAbsolutePath = self::$projectDir . $origImgPathNoExt;

        foreach ($dimensions as $key => $heightDimension) {
            $this->loadImage(self::$projectDir . $origImgPath);
            self::$image->resizeToHeight($heightDimension);
        
            $newImagePath = $imgAbsolutePath . "-" . $key;
            self::$image->save($newImagePath . ".jpg");
            $this->convertImageToWebP($newImagePath . ".jpg", $newImagePath . ".webp");
        
            $imgDimensionsPath[$key] = $origImgPathNoExt . "-" . $key;
        }
        return $imgDimensionsPath;
    }

    public function loadImage($path)
    {
        self::$image->init($path);
    }

    public function convertImageToWebP($source, $destination, $quality = 80) {
        $extension = pathinfo($source, PATHINFO_EXTENSION);
        if ($extension == 'jpeg' || $extension == 'jpg') 
            $image = imagecreatefromjpeg($source);
        elseif ($extension == 'gif') 
            $image = imagecreatefromgif($source);
        elseif ($extension == 'png') 
            $image = imagecreatefrompng($source);

        imagewebp($image, $destination, $quality);
        // Free up memory
        imagedestroy($image);
    }


    private function getImageSuffix($fileTmpName)
    {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->file($fileTmpName);
        switch ($mimeType) {
            case 'image/png':
                return '.png';
            case 'image/jpg':
                return '.jpg';
            case 'image/gif':
                return '.gif';
            default:
                return '.jpg';
        }
    }

    public function controlImage($image)
    {
        if (!isset($image)) {
            throw new Exception("Control image => invalid index", 1);
              
        }
        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if ( !isset($image['error']) || is_array($image['error'])) {
            throw new Exception('Invalid parameters.');
        }

        // Check $image['error'] value.
        switch ($image['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new UserException('Žádný soubor nebyl zadán');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new UserException('Byl přesažen limit pro velikost obrázku.');
            default:
                throw new Exception('Unknown errors.');
        }

        // You should also check filesize here.
        // 1 000 000 = 1 MB
        if ($image['size'] > 5000000) {
            throw new UserException('Byl přesažen limit pro velikost obrázku.');
        }

        // DO NOT TRUST $image['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
            $finfo->file($image['tmp_name']),
            array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
            ),
            true
        )) {
            throw new UserException('Špatný typ obrázku, povolené jsou pouze: JPG,PNG,GIF');
        }
    }

    public function getFilePath()
    {
        return "/public/media";
    }

    public function getImagePath()
    {
        return self::$projectDir . $this->getFilePath();
    }

    public function safeFileName($name)
    {
        return StringUtils::hyphenize($name);
    }
}