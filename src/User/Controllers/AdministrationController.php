<?php

namespace App\CoreModule\User\Controllers;

use App\CoreModule\User\Models\UserManager;
use App\CoreModule\User\Models\AdministrationManager;
use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use App\CoreModule\System\Controllers\EmailSenderController;
use Utils\EmailSender;
use Settings;

/**
 * Process request from user administration 
 */
class AdministrationController extends Controller
{
    /**
     * Manager for article
     * @var ArticleManager
     */
    public $articleManager;

    /**
     * Class for manage user
     * @var UserManager
     */
    public $userManager;

    function __construct(
        ArticleManager $articleManager,
        UserManager $userManager,
        AdministrationManager $administrationManager
    )
    {
        $this->articleManager = $articleManager;
        $this->userManager = $userManager;
        $this->administrationManager = $administrationManager;
    }

    /**
     * @Action
     * User registration
     */
    public function index()
    {
        // Control is user login 
        $this->authUser();

        // Init forms
        $this->userDataForm();
        $this->changeUserPasswordForm();


        // Naplnění proměnných pro šablonu
        $this->data['title'] = $this->articleManager->article['title'];
        $this->data['content'] = $this->articleManager->article['content'];
        $this->data['user'] = $this->userManager->user;

        // Nastavení šablony
        $this->view = 'index';
    }

    /**
     * @Action
     */
    public function logout()
    {
        $this->logOutUser();
    }

    /**
     * @Action
    */
    public function deleteUser()
    {
        try {
            $this->userManager->deleteUser();
            unset($_SESSION['user']);
            $this->addMessage('Učet byl úspěšně smazán' , "success");
            $this->redirect('/');
        } catch (UserException $ex) {
             $this->addMessage($ex->getMessage());
        }
    }

    /**
     * @Action
     * Verification email
     */
    public function sendEmailVerification()
    {
        // Control is user login 
        $this->authUser();

        try
        {
            $emailSender = new EmailSenderController();
            $address = $this->userManager->user['email'];
            $verificationLink = "http://".Settings::$domain . "/administrace/control-verification-email-link/" . $this->userManager->generateEmailVerificationLink();
            $subject = $this->translate("Ověření emailu na webu") . " " .  Settings::$domain;
            $message = $emailContent = $this->translate('Po kliknutí na') .' <a href="' . $verificationLink . '"> '.$this->translate('odkaz') .' </a> '. $this->translate('budete přesměrování na stránku kde proběhne ověření emailu.');
            $emailSender->sentEmailSMTP($address, $subject, $message);
            $this->addMessage($this->translate('Ověření bylo zasláno na email') . ' ' . $email . '. '. $this->translate('Pro ověření klikněte na odkaz zaslaný v emailu.') , "success");
            $this->redirect('administrace');
        }
        catch (UserException $ex)
        {
            $this->addMessage($ex->getMessage());
        }
    }   

    /**
    * @Action
    * Control verification link from email
    */
    public function controlVerificationEmailLink($link)
    {
        $verification = $this->userManager->controlVerificationEmailLink($link);

        if (!$verification) {
            $this->addMessage($this->translate('Odkaz pro ověření emailu je neplatný.') , "danger");
            $this->redirect('/');
        } 
        $this->addMessage($this->translate('Email byl úspěšně ověřen') , "success");
        $this->redirect('/');
    }

    public function userDataForm()
    {
        $userForm = $this->administrationManager->getUserForm();
        $userForm->setData($this->userManager->user);
        $this->data['userForm'] = $userForm;

        if ($userForm->isPostBack())
        {
            try
            {
                $data = $userForm->getData();
                $this->userManager->update($data);
                $this->addMessage($this->translate('Váš profil byl upraven'));
                $this->redirect('administrace');
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage());
            }
        }
    }

    public function changeUserPasswordForm()
    {
        $changePasswordForm = $this->administrationManager->getChangePasswordForm();
        $this->data['changePasswordForm'] = $changePasswordForm;

        if ($changePasswordForm->isPostBack())
        {
            try
            {
                $data = $changePasswordForm->getData();
                $this->userManager->passwordUpdate($data);
                $this->addMessage($this->translate('Heslo bylo úspěšně upraveno'));
                $this->redirect('administrace');
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage(), 'danger');
            }
        }
    }
}