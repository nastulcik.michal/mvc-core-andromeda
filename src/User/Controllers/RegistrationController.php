<?php

namespace App\CoreModule\User\Controllers;

use App\CoreModule\User\Models\UserManager;
use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use Utils\HtmlBuilder;
use Utils\Forms\Form;
use Utils\UserException;
use Settings;

class RegistrationController extends Controller
{
    /**
     * Manager for article
     * @var ArticleManager
     */
    public $articleManager;

    /**
     * Class for manage user
     * @var UserManager
     */
    public $userManager;

    function __construct(
        ArticleManager $articleManager,
        UserManager $userManager
    )
    {
        $this->articleManager = $articleManager;
        $this->userManager = $userManager;
    }

    /**
     * @Action
     * User registration
     */
    public function index()
    {
        $form = $this->registrationForm();
        $this->data['form'] = $form;

        if ($form->isPostBack())
        {
            try
            {
                $data = $form->getData();
                $this->userManager->register($data);
                $this->userManager->login($data['username'], $data['password']);
                $this->addMessage('Byl jste úspěšně zaregistrován.');
                $this->redirect('administrace');
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage());
            }
        }

        $this->data['title'] = $this->articleManager->article['title'];
        $this->data['content'] = $this->articleManager->article['content'];

        // Set template
        $this->view = 'index';
    }

    public function registrationForm()
    {
        $form = new Form('registration');
        
        $form->addTextBox('username', 'Uživatelské jméno', true, ['class' => 'form-control-sm','placeholder' => 'Pepíno']);
        
        $form->addPasswordBox('password', 'Heslo', true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");
        
        $form->addPasswordBox('password_repeat', 'Heslo', true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");

        $form->addTextBox('firstname', 'Křestní jméno', true, ['class' => 'form-control-sm','placeholder' => 'Pepa']);

        $form->addTextBox('surname', 'Příjmení', true, ['class' => 'form-control-sm','placeholder' => 'Trávníček',]);

        $form->addTextBox('phone', 'Telefon', true, ['class' => 'form-control-sm','placeholder' => "123 456 789",]);

        $form->addEmailBox('email', 'Vaše emailová adresa', true, ['class' => 'form-control-sm','placeholder' => "papa.travnicek@pepovo.cz"]);

        $form->addCheckBox('advertising_emails' , 'Souhlasím se zasíláním reklamních emailů.', false);

        $form->addCheckBox('gdpr' , 'Souhlasím se zpracováním osobních údajů.', true);
        
        $form->addRecaptchaBox(Settings::$recaptcha['secret_key'], Settings::$recaptcha['site_key'], 0.8);

        $form->addButton('submit' , 'Odeslat registraci', ['class' => 'btn-sm btn-warning rounded-pill float-right mt-3']);

        return $form;
    }
}