<?php

namespace App\CoreModule\Articles\Controllers;

use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use App\CoreModule\User\Models\UserManager;
use Utils\Utility\ArrayUtils;
use Utils\UserException;
use Utils\Forms\Form;

/**
 * Process request on edit article
 */
class EditorController extends Controller
{
    public $articleManager;

    /**
     * Article controller instnce
     * @var Controller
     */
    protected $controller;

    function __construct(
        UserManager $userManager,
        ArticleManager $articleManager
    )
    {
        $this->userManager = $userManager;
        $this->articleManager = $articleManager;
    }

    /**
     * @Action
     * Edit article
     * @param array article 
     */
    public function edit($articleUrl = "")
    {
        // Control login
        $this->authUser();

        $form = $this->getEditForm();

        if ($form->isPostBack())
        {
            try
            {
                $data = $form->getData();
                if (!$this->userManager->user['super_admin']) {
                    unset($data['controller']);
                }
                $this->articleManager->saveArticle($data);
                $this->addMessage('Článek byl úspěšně uložen', 'success');
                $this->redirect('editor/edit/'. $data['url']);
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage(), 'danger');
            }
        }

        // Prepare variables for template
        $this->data['title'] = $this->articleManager->article['title'];
        $article = $this->articleManager->getArticle($articleUrl);
        
        $this->data['article'] = ($article) ? $article : [];
        $this->data['controllers'] = ($this->userManager->user['super_admin']) ? $this->getControllers() : [];

        $form->setData($this->data['article']);
        $this->data['form'] = $form;

        // Set template
        $this->view = 'article-editor';
    }


    /**
     * @Action
     * Delete article
     * @param array article 
     */
    public function delete($articleUrl)
    {
        // Control login
        $this->authUser();

        try
        {
            $this->articleManager->deleteArticle($articleUrl);
            $this->addMessage('Článek byl úspěšně smazán', 'success');
            $this->redirect('seznam-clanku');
        }
        catch (UserException $ex)
        {
            $this->addMessage($ex->getMessage(), 'danger');
        }
    }

    /**
     * Return edit form
     * @return Form Edit form
     */
    public function getEditForm()
    {
        $form = new Form('edit-article');
        $form->addHiddenBox('article_id');
        $form->addTextBox('title', 'Titulek', true, ['class' => "form-control"]);
        $form->addTextArea('content', 'Obsah', false, ['class' => "form-control"]);
        $form->addTextBox('url', 'Url', true, ['class' => "form-control"]);
        $form->addTextBox('description', 'Popisek', true, ['class' => "form-control"]);
        $form->addTextBox('controller', 'Controller', false, ['class' => "form-control hidden", 'autocomplete' => "off"]);
        $form->addCheckBox('show_title', 'Zobrazit titulek', true);
        $form->addCheckBox('full_width', 'Na celou šířku stránky', false);
        $form->addButton('submit' , 'Odeslat', ['class' => "form-control"]);
        return $form;
    }

    public function getControllers()
    {
        return ArrayUtils::mapSingles($this->articleManager->getControllers(), 'controller');
    }
}