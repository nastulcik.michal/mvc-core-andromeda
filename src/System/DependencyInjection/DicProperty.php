<?php 
namespace App\CoreModule\System\DependencyInjection;

/**
 * DIC - dependency injection container type = INJECTION PROPERTY
 */
class DicProperty
{
	private $services = [];

	public function addService($service)
	{
        $key = get_class($service);
        $this->services[$key] = $service;
	}

	public function returnInstance($fullClassName)
	{
        // Vytvoření instance
        if (!isset($this->services[$fullClassName]))
        {
            $instance = new $fullClassName;

            $this->services[$fullClassName] = $instance;
            // Injection dependencies
            $reflection = new \ReflectionClass($instance);
            $this->injectDependencies($reflection, $instance);
            return $instance;
        }

        return $this->services[$fullClassName];
	}

	private function injectDependencies($reflection, $instance)
	{
        $properties = $reflection->getProperties();

        foreach ($properties as $property)
        {
            $commentary = $property->getDocComment();
            // Nalezení vlastnosti, do které se má provést "injection"
            if (strpos($commentary, '@Inject') !== false)
            {
                // Vyparsování typu vlastnosti z anotace
                $match = [];
                if (!preg_match('~@var\s+([A-Za-z0-9\\\_\-]+)~u', $commentary, $match))
                        throw new Exception('Nepodařilo se zjistit datový typ vlastnosti ' . $property->getName());
                $type = $match[1];
                $property->setValue($instance, $this->returnInstance($type)); // Zde provádíme "injection"
            }
        }
	}

}