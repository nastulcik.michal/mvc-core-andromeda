<?php

namespace App\CoreModule\User\Models;

use Utils\UserException;
use Utils\Utility\StringUtils;
use Utils\HtmlBuilder;
use Utils\Forms\Form;
use PDOException;

class AdministrationManager
{

    public function getUserForm()
    {
        $form = new Form('edit-user');
        $form->addTextBox('username', 'Uživatelské jméno', true, ['class' => 'form-control-sm']);
        $form->addTextBox('firstname', 'Křestní jméno', true, ['class' => 'form-control-sm']);
        $form->addTextBox('surname', 'Příjmení', true, ['class' => 'form-control-sm']);
        $form->addTextBox('phone', 'Telefon', true, ['class' => 'form-control-sm']);
        $form->addEmailBox('email', 'Vaše emailová adresa', true, ['class' => 'form-control-sm']);

        $form->addButton('submit' , 'Uložit změny', ['class' => 'btn-sm btn-warning rounded-pill float-right mt-3']);
        return $form;
    }

    public function getChangePasswordForm()
    {
        $form = new Form('change-password');

        $form->addPasswordBox('old_password', 'Aktuální heslo', true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");
        
        $form->addPasswordBox('new_password', 'Nové heslo', true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");

        $form->addPasswordBox('new_password_repeat', 'Kontrola nového hesla', true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");

        $form->addButton('submit' , 'Změnit heslo', ['class' => 'btn-sm btn-warning rounded-pill mt-3']);

        return $form;
    }
}   