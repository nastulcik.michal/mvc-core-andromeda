<?php

/**
 * Wrapper for translate text in system
 */
class TranslateHelper {

	/**
	 * @var Translate template
	 */
	public static $library;

	/**
	 * @param string $langCode Language code for translate 
	*/
	public static $transLang = 'cz';

	public static $enableLang = ['sk','eng','pl','hun'];

	public function init($db)
	{
		if (isset($_SESSION['lang']) && in_array($_SESSION['lang'], self::$enableLang)) {
			self::$transLang = $_SESSION['lang'];
		}
		$this->prepareData($db);
	}

	/**
	 * Prepare data for translate text in application
	 */
	private function prepareData($db) {
		if (!self::$library) {
            $res = $db->queryAll('SELECT `cz`,`'.self::$transLang.'` FROM trans_library WHERE `trans_code` IS NULL AND `cz` IS NOT NULL');
			foreach ($res as $key => $val) {
				self::$library[$val['cz']] = $val[self::$transLang];
			}

            $res = $db->queryAll('SELECT `trans_code`,`'.self::$transLang.'` FROM trans_library WHERE `trans_code` IS NOT NULL');
            foreach ($res as $key => $val) {
                self::$library[$val['trans_code']] = $val[self::$transLang];
            }
		}
	}

	public static function get($text)
	{
		if (isset(self::$library[$text])) {
			return self::$library[$text];
		}
		return $text;
	}
	
	/**
	 * Create annonymous class for use in template view
	 */
	public static function getLibraryClass()
	{
		$anonClass = new class("trans") extends TranslateHelper {};
		$anonClass::$transLang = self::$transLang;
		$anonClass::$enableLang = self::$enableLang;
		return $anonClass;
	}
}
