<?php

namespace App\CoreModule\Articles\Models;

use Utils\UserException;
use Utils\ArrayToXml;
use PDOException;
use DbHelper;

/**
 * Class for manipulate with articles in system
 */
class ArticleManager
{

    /**
     * Actual load article
     * @var array
     */
    public $article;

    /**
     * Db wrapper
     * @var DbHelper
     */
    public $db;

    function __construct(DbHelper $db, ArrayToXml $arr)
    {
        $this->db = $db;
    }

    /**
     * Load article from db and save to $article property
     * @param string $url article URL
     */
    public function loadArticle($url)
    {
        $this->article = $this->getArticle($url);
    }

    /**
     * Return article by url
     * @param string $url article URL
     * @return mixed Array with article or false
     */
    public function getArticle($url)
    {
        return $this->db->queryOne('
            SELECT *
            FROM `article`
            WHERE `url` = ?
        ', [$url]);
    }

    /**
     * Save article to db 
     * @param array $article Array with article
     */
    public function saveArticle($article)
    {
        if (!$article['article_id'])
        {
            try
            {
                // Delete article element - empty element
                unset($article['article_id']);
                $date = date("Y-m-d H:i:s");
                $article['updated_at'] = $date;
                $article['created_at'] = $date;
                $this->db->insert('article', $article);
            }
            catch (PDOException $ex)
            {
                throw new UserException('Článek s touto URL adresou již existuje.');
            }
        }
        else{
            $article['updated_at'] = date("Y-m-d H:i:s");
            $this->db->update('article', $article, 'WHERE article_id = ?', [$article['article_id']]);
        }
    }

    /**
     * Return article list
     * @return mixed Array with articles or false
     */
    public function getArticles($superAdmin = false)
    {
        $where = "WHERE controller IS NULL OR controller = ''";
        
        // When is user superadmin show article with controller
        if ($superAdmin)
            $where = '';

        return $this->db->queryAll("
            SELECT *
            FROM `article`
            " . $where . "
            ORDER BY `article_id` ASC
        ");
    }

    /**
     * @param string $url article URL
     */
    public function deleteArticle($url)
    {
        $this->db->query('DELETE FROM article WHERE url = ?', [$url]);
    }

    public function getControllers()
    {
        return $this->db->queryAll("
            SELECT DISTINCT(controller)
            FROM `article`
            WHERE controller IS NOT NULL AND controller != ''
        ");
    }
}