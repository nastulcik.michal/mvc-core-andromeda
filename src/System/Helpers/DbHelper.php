<?php

/**
 * Wrapper pro snadnější práci s databází s použitím PDO a automatickým
 * zabezpečením parametrů (proměnných) v dotazech.
 */
class DbHelper {

  /**
   * @var PDO Databázové spojení
   */
  private $connection;

  /**
   * @var array Výchozí nastavení ovladače
   */
  private $settings = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_EMULATE_PREPARES => false,
  ];

  /**
   * Připojí se k databázi pomocí daných údajů
   * @param string $host Hostitel
   * @param string $user Uživatelské jméno
   * @param string $password Heslo
   * @param string $database Název databáze
   */
  public function connect($host, $user, $password, $database) {
    if (!isset($this->connection)) {
      $this->connection = @new PDO(
        "mysql:host=$host;dbname=$database",
        $user,
        $password,
        $this->settings
      );
    }
  }

  public function beginTransaction()
  {
    $this->connection->beginTransaction();
  }

  public function commit()
  {
    $this->connection->commit();
  }

  public function rollBack()
  {
    $this->connection->rollBack();
  }

  /**
   * Spustí dotaz a vrátí z něj první řádek
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return mixed Pole výsledku nebo false
   */
  public function queryOne($query, $parameters = array(), $fetchStyle = PDO::FETCH_ASSOC) {
    $statement = $this->connection->prepare($query);
    $statement->execute($parameters);
    return $statement->fetch($fetchStyle);
  }

  /**
   * Spustí dotaz a vrátí z něj všechny řádky
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return mixed Pole výsledků nebo false
   */
  public function queryAll($query, $parameters = array(), $fetchStyle = PDO::FETCH_ASSOC) {
    $statement = $this->connection->prepare($query);
    $statement->execute($parameters);
    return $statement->fetchAll($fetchStyle);
  }

  /**
   * Spustí dotaz a vrátí z něj první sloupec prvního řádku
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return mixed První hodnota výsledku dotazu nebo false
   */
  public function querySingle($query, $parameters = array()) {
    $statement = $this->queryOne($query, $parameters);
    return $statement ? reset($statement) : false;
  }

  /**
   * Spustí dotaz a vrátí počet ovlivněných řádků
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return int Počet ovlivněných řádků
   */
  public function query($query, $parameters = array()) {
    $statement = $this->connection->prepare($query);
    $statement->execute($parameters);
    return $statement->rowCount();
  }

  /**
   * Vloží do tabulky nový řádek jako data z asociativního pole
   * @param string $table Název tabulky
   * @param array $parameters Asociativní pole s daty
   * @return int Počet ovlivněných řádků
   */
  public function insert($table, $parameters = array()) {
    return $this->query("INSERT INTO `$table` (`".
      implode('`, `', array_keys($parameters)).
      "`) VALUES (".str_repeat('?,', sizeOf($parameters)-1)."?)",
      array_values($parameters));
  }

  /**
   * Změní řádek v tabulce tak, aby obsahoval data z asociativního pole
   * @param string $table Název tabulky
   * @param array $values Asociativní pole s daty
   * @param $condition Část SQL dotazu s podmínkou včetně WHERE
   * @param array $parameters Parametry dotazu
   * @return int Počet ovlivněných řádků
   */
  public function update($table, $values = array(), $condition, $parameters = array()) {
    return $this->query("UPDATE `$table` SET `".
      implode('` = ?, `', array_keys($values)).
      "` = ? " . $condition,
      array_merge(array_values($values), $parameters));
  }

  public function delete($table, $condition, $parameters = [])
  {
    return $this->query("DELETE FROM `$table` " . $condition, $parameters);
  }

  /**
   * Return simply array by keyColumn and ValueColumn
   * @param  string $query       Sql query
   * @param  string $keyColumn   Key index name
   * @param  string $valueColumn Value index name
   * @param  array  $parameters  Parameters
   * @return array              Simply array index by key Column
   */
  public function queryPairs($query, $keyColumn, $valueColumn, $parameters = array())
  {
      return ArrayUtils::mapPairs($this->queryAll($query, $parameters), $keyColumn, $valueColumn);
  }

  /**
   * @return string Vrací ID posledně vloženého záznamu
   */
  public function getLastId()
  {
    return $this->connection->lastInsertId();
  }

  /**
   * Multiple insert to table
   * @param  string $table      Table name
   * @param  array  $parameters Parameters
   * @return boolean              
   */
  public function insertAll($table, $parameters = array() , $maxCountInsert = 1000)
  {
    if(count($parameters) < $maxCountInsert){
      return $this->insertAllToDb($table, $parameters);
    }
    $countParamaters = count($parameters);
    $countPackages = (int)($countParamaters / $maxCountInsert);
    for ($i=1; $i <= $countPackages; $i++) { 
      $beginIndex = $maxCountInsert * ($i-1);
      $endIndex = $beginIndex + $maxCountInsert;
      $package = array_slice($parameters, $beginIndex, $maxCountInsert);
        
      echo "Insert data package(" .$maxCountInsert . ") to db ( ". $beginIndex . "/" . $endIndex . " from " . $countParamaters ."): " . count($package). "\n";
      $this->insertAllToDb($table, $package);
    }

    if($endIndex < $countParamaters ){
      echo "Insert rest of the data \n";
      $this->insertAllToDb($table, array_slice($parameters, $endIndex));
    }

    return true;
  }


  private function insertAllToDb($table, $parameters = array())
  {
      $params = array();
      $query = rtrim("INSERT INTO `$table` (`".
          implode('`, `', array_keys($parameters[0])).
          "`) VALUES " . str_repeat('(' . str_repeat('?,', sizeOf($parameters[0])-1)."?), ", sizeOf($parameters)), ', ');
      foreach ($parameters as $rows)
      {
          $params = array_merge($params, array_values($rows));
      }
      return $this->query($query, $params);
  }
}
