<?php

namespace App\CoreModule\System\Controllers;

use PHPMailer\PHPMailer;
use PHPMailer\SMTP;
use PHPMailer\Exception;
use Settings;

/**
 * Helper class for send email
 */
class EmailSenderController
{

	/**
	 * Odešle email jako HTML, lze tedy používat základní HTML tagy a nové
	 * řádky je třeba psát jako <br /> nebo používat odstavce. Kódování je
	 * odladěno pro UTF-8.
	 * @param string $address Adresa
	 * @param string $subject Předmět
	 * @param string $message Zpráva
	 * @param string $from Adresa odesílatele
	 * @throws UserException
	 */
	public function send($address, $subject, $message, $from)
	{
		$header = "From: " . $from;
		$header .= "\nMIME-Version: 1.0\n";
		$header .= "Content-Type: text/html; charset=\"utf-8\"\n";
		if (Settings::$debug)
		{
			file_put_contents('files/emails/' . uniqid(), $message);
			return true;
		}
		if (!mb_send_mail($address, $subject, $message, $header))
			throw new UserException('Email se nepodařilo odeslat.');
	}

	/**
	 * Zkontroluje, zda byl zadán aktuální rok jako antispam a odešle email
	 * @param int $year Aktuální rok
	 * @param string $address Adresa
	 * @param string $subject Předmět
	 * @param string $message Zpráva
	 * @param $from Adresa odesílatele
	 * @throws UserException
	 */
	public function sendWithAntispam($year, $address, $subject, $message, $from)
	{
		if ($year != date("Y"))
			throw new UserException('Chybně vyplněný antispam.');
		$this->send($address, $subject, $message, $from);
	}

	/**
	 * Sent email by SMTP 
	 * @throws UserException
	 */
	public function sentEmailSMTP($from, $address, $subject, $message)
    {
        if (Settings::$debug)
        {
            // file_put_contents('files/emails/' . uniqid(), $message);
        }
        if (!Settings::$SMTP) {
            if (Settings::$debug){
                throw new UserException('Email se nepodařilo odeslat. Není nastaven SMTP server');
            } else {
                throw new UserException('Omlouváme se, ale aktuálně není možné email odeslat. Zkuste to prosím později.');
            }
        }

        $MAIL = new PHPMailer\PHPMailer(TRUE);
        
        try {
            $MAIL->setFrom($from);
            $MAIL->CharSet = 'utf-8';
            $MAIL->addAddress($address);
            $MAIL->Subject = $subject;

            $MAIL->isHTML(TRUE);
            $MAIL->Body = $message;

            /* použijeme SMTP */
            $MAIL->isSMTP();
               
            /* SMTP server */
            $MAIL->Host = Settings::$SMTP['host'];

            $MAIL->SMTPAuth = TRUE;

            // /* Encryption */
            $MAIL->SMTPSecure = 'tls';

            //  SMTP username 
            $MAIL->Username = Settings::$SMTP['username'];

            // /* SMTP heslo */
            $MAIL->Password = Settings::$SMTP['password'];

            /* SMTP port */
            $MAIL->Port = 587;
            $result = $MAIL->send();
            unset($MAIL);   
        }
        catch (UserException $e)
        {
            die($e->getMessage());
            throw new UserException($e->getMessage());
        }
    }
}