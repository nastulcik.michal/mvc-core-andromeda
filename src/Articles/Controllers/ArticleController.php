<?php

namespace App\CoreModule\Articles\Controllers;

use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use DependencyInjectionConstruct;

/**
 * Process request on article
 */
class ArticleController extends Controller
{
    public $articleManager;

    /**
     * Article controller instnce
     * @var Controller
     */
    protected $controller;

    function __construct(
        DependencyInjectionConstruct $DIconstruct,
        ArticleManager $articleManager
    )
    {
        $this->DIconstruct = $DIconstruct;
        $this->articleManager = $articleManager;
    }

    /**
     * @Action
     * Load article or controller
     * @param array $parameters Array with params for nested controller
     */
    public function index($parameters)
    {
        // Get article by URL
        $this->articleManager->loadArticle($parameters[0]);

        // If article dont find, redirect to ErrorController
        if (!$this->articleManager->article) {
            http_response_code(404);
            // HOTFIX for undefined url - 404
            $this->articleManager->loadArticle("chyba");
            // Set variables for template
            $this->data['title'] = "404";
            // Show h1 on page or not
            $this->data['showTitle'] = 0;
            $controller = $this->DIconstruct->returnInstance('App\SexCzech\Pages\Controllers\PageNotFoundController');
            $controller->callActionFromParams(['index']);
            $this->data['controller'] = $controller;
        } else {
            // Call nested controller
            if ($this->articleManager->article['controller']) {
                $fullName = 'App\\' . $this->articleManager->article['controller'] . 'Controller';
                $controller = $this->DIconstruct->returnInstance($fullName);
                array_shift($parameters); // mezi parametry nepatří URL článku

                if (isset($this->articleManager->article['class_method']) && $this->articleManager->article['class_method']) {
                    array_unshift($parameters, $this->articleManager->article['class_method']);
                }

                try {
                    $controller->callActionFromParams($parameters);
                } catch (\Exception $exception) {
                    \Sentry\captureException($exception);
                    http_response_code(404);
                    // Set variables for template
                    $this->data['title'] = "404";
                    // Show h1 on page or not
                    $this->data['showTitle'] = 0;
                    $controller = $this->DIconstruct->returnInstance('App\SexCzech\Pages\Controllers\PageNotFoundController');
                    $controller->callActionFromParams(['index']);
                }


                $this->data['controller'] = $controller;
            }
            // Only article
            else {
                $this->data['controller'] = null;
            }
        }


        // Set variables for template
        $this->data['title'] = $this->translate($controller->data['title']??$this->articleManager->article['title']);
        $this->data['description'] = $this->translate($controller->data['description']??$this->articleManager->article['description']);
        // Show h1 on page or not
        $this->data['showTitle'] = $this->translate($this->articleManager->article['show_title']);

        $this->data['content'] = $this->translate($this->articleManager->article['content']);
        // Set template
        $this->view = 'index';

        return $this;
    }
}